package com.tiseno.ccrewards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.BankDetail;
import com.tiseno.ccrewards.webservicedata.ListViewImageLoaderTask;
import com.tiseno.ccrewards.webservicedata.SoapResultParser;
import com.tiseno.ccrewards.webservicedata.WebServiceParsingTask;

public class SearchByBankActivity extends SherlockActivity implements
		AsyncTaskCompleteListener<SoapObject> {
	private List<BankDetail> banksData = new ArrayList<BankDetail>();
	private List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();

	private ListView lv_bank;

	ProgressBar dialog;

	String[] from = { "bankname", "image" };
	int[] to = { R.id.bt_bank_search_bankname, R.id.bt_bank_search_bankimage };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Global.setActionBar(this, getResources().getString(R.string.search));
		setContentView(R.layout.activity_searchbybank);
		if (Global.haveNetworkConnection(this))
			initView();
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onTaskComplete(SoapObject result) {
		for (int index = 0; index < result.getPropertyCount(); index++) {
			SoapObject childObj = (SoapObject) result.getProperty(index);
			BankDetail _bankDetail = new BankDetail();
			SoapResultParser soapResult = new SoapResultParser();
			try {
				soapResult.parseDataObject(childObj.toString(), _bankDetail);
				banksData.add(_bankDetail);
				HashMap<String, Object> _data = new HashMap<String, Object>();
				_data.put("bankname", _bankDetail.bankname﻿);
				_data.put("image", android.R.color.transparent);
				_data.put("teaser_path", _bankDetail.bankimage﻿);

				Log.i(Global.TAG, "childObj " + childObj.toString());
				data.add(_data);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// dialog.dismiss();
		dialog.setVisibility(View.GONE);
		LoadListView();
	}

	private void LoadListView() {
		SimpleAdapter adapter = new SimpleAdapter(this, data,
				R.layout.button_bank_search, from, to);
		lv_bank.setAdapter(adapter);

		for (int i = 0; i < adapter.getCount(); i++) {
			HashMap<String, Object> hm = (HashMap<String, Object>) adapter
					.getItem(i);
			String imgUrl = (String) hm.get("teaser_path");

			ListViewImageLoaderTask imageLoaderTask = new ListViewImageLoaderTask(
					this, lv_bank);

			HashMap<String, Object> hmDownload = new HashMap<String, Object>();
			String url = Global.IMAGE_URL + imgUrl;
			hm.put("teaser_path", url);
			hm.put("position", i);

			// Starting ImageLoaderTask to download and populate image in
			// the listview
			imageLoaderTask.execute(hm);
		}
	}

	private void initView() {
		ButtonListerner bl = new ButtonListerner(this);
		bl.setListeners();
		bl.setActiveButton(bl.SEARCH);

		Button bt_searchByBank = (Button) findViewById(R.id.st_search_bank);
		bt_searchByBank.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.button_click));
		bt_searchByBank.setClickable(false);
		bt_searchByBank.setTypeface(Global.getTypeface(this,
				"Wisdom Script.otf"));

		Button bt_searchByMerchant = (Button) findViewById(R.id.st_search_merchan);
		bt_searchByMerchant.setTypeface(Global.getTypeface(this,
				"Wisdom Script.otf"));
		bt_searchByMerchant.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SearchByBankActivity.this,
						SearchByMerchantActivity.class);
				startActivity(intent);
				SearchByBankActivity.this.finish();
			}
		});

		lv_bank = (ListView) findViewById(R.id.act_search_bank_lv);
		lv_bank.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView adapterView, View view,
					int position, long id) {
				SimpleAdapter adapter = (SimpleAdapter) adapterView
						.getAdapter();

				HashMap<String, Object> item = (HashMap<String, Object>) adapter
						.getItem(position);

				BankDetail bd = banksData.get(position);
				Intent intent = new Intent(SearchByBankActivity.this,
						SearchBankCardActivity.class);
				intent.putExtra("bankid", bd.bankid﻿);
				intent.putExtra("bankname", bd.bankname﻿);
				startActivity(intent);
			}
		});

		getAllCat();

	}

	private void getAllCat() {
		WebServiceParsingTask webTask = new WebServiceParsingTask(
				Global.WSDL_URL, Global.NAMESPACE,
				Global.SOAPMETHOD_GETALLBANK, null,
				Global.SOAPACTION_GETALLBANK, this);

		webTask.execute();

		// dialog = ProgressDialog.show(
		// SearchByBankActivity.this,
		// "",
		// SearchByBankActivity.this.getResources().getString(
		// R.string.loading_dialog), true);
		dialog = (ProgressBar) findViewById(R.id.progressBar);
		dialog.setVisibility(View.VISIBLE);
	}

	// back press exit
	boolean doubleBackToExitPressedOnce = false;

	@Override
	public void onBackPressed() {
//		if (doubleBackToExitPressedOnce) {
		Intent intent = new Intent(this, HotDealActivity.class);
		this.startActivity(intent);
		this.finish();
//		super.onBackPressed();
		return;
//		}
//		this.doubleBackToExitPressedOnce = true;
//		Toast.makeText(this,
//				getResources().getString(R.string.press_back_alert),
//				Toast.LENGTH_SHORT).show();
//		new Handler().postDelayed(new Runnable() {
//
//			@Override
//			public void run() {
//				doubleBackToExitPressedOnce = false;
//
//			}
//		}, 2000);
	}

}
