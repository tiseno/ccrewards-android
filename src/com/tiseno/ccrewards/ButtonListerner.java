package com.tiseno.ccrewards;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewDebug.FlagToString;
import android.widget.Button;

import com.tiseno.ccrewards.R;

public class ButtonListerner {
	// activity code
	public static final int SEARCH = 100;

	public static final int FAVOIRITE = 110;

	public static final int HOTDEAL = 120;

	public static final int HOTDEAL_other = 121;

	public static final int MYCARD = 130;

	public static final int INFO = 140;

	// activity code
	Activity activity, curActivity;
	int activity_code;
	Button bt_search, bt_favourite, bt_mycard, bt_info, bt_hotdeal;
	
	//share msg
	String shareBody = "";

	int[] avctiveImage = { R.drawable.ic_nav_search_active,
			R.drawable.ic_nav_favorites_active,
			R.drawable.ic_nav_hotdeal_active, R.drawable.ic_nav_cards_active,
			R.drawable.ic_nav_info_active };

	int[] buttonId = { R.id.bm_bt_search, R.id.bm_bt_favourite,
			R.id.bm_bt_hotdeal, R.id.bm_bt_mycard, R.id.bm_bt_info };

	public ButtonListerner(Activity activity) {
		this.activity = activity;
	}
	
	public void setShareMsg(String msg) {
		this.shareBody = msg;
	}

	public void setListeners() {
		bt_search = (Button) activity.findViewById(R.id.bm_bt_search);
		bt_search.setOnClickListener(SearchListener);

		bt_favourite = (Button) activity.findViewById(R.id.bm_bt_favourite);
		bt_favourite.setOnClickListener(FavouriteListener);

		bt_mycard = (Button) activity.findViewById(R.id.bm_bt_mycard);
		bt_mycard.setOnClickListener(MycardListener);

		bt_info = (Button) activity.findViewById(R.id.bm_bt_info);
		bt_info.setOnClickListener(InfoListener);

		bt_hotdeal = (Button) activity.findViewById(R.id.bm_bt_hotdeal);
		bt_hotdeal.setOnClickListener(HotdealListener);

	}

	OnClickListener SearchListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(activity, SearchByBankActivity.class);
			activity.startActivity(intent);
//			activity.finish();
		}
	};

	OnClickListener FavouriteListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			curActivity = activity;
			Intent intent = new Intent(activity, FavouriteActivity.class);
			activity.startActivity(intent);
//			activity.finish();
		}
	};

	OnClickListener MycardListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(activity, MyCardActivity.class);
			activity.startActivity(intent);
//			activity.finish();
		}
	};

	OnClickListener InfoListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(activity, InfoActivity.class);
			activity.startActivity(intent);
//			activity.finish();
		}
	};

	OnClickListener HotdealListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent;
//			if (activity_code == HOTDEAL_other) {
//				activity.finish();
//			}else if(activity_code != HOTDEAL){
				intent = new Intent(activity, HotDealActivity.class);
				activity.startActivity(intent);
//				activity.finish();
//			}
		}
	};
	
	OnClickListener ShareListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(android.content.Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
			activity.startActivity(Intent.createChooser(intent, "Share via"));
		}
	};
	
	OnClickListener AddFavouriteListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
		}
	};

	public void setActiveButton(int id) {
		activity_code = id;
		switch (id) {
		case SEARCH:
			bt_search.setBackgroundResource(avctiveImage[0]);
//			bt_search.setClickable(false);
			break;

		case FAVOIRITE:
			bt_favourite.setBackgroundResource(avctiveImage[1]);
//			bt_favourite.setClickable(false);
			break;

		case HOTDEAL:
		case HOTDEAL_other:
			bt_hotdeal.setBackgroundResource(avctiveImage[2]);
			break;

		case MYCARD:
			bt_mycard.setBackgroundResource(avctiveImage[3]);
//			bt_mycard.setClickable(false);
			break;

		case INFO:
			bt_info.setBackgroundResource(avctiveImage[4]);
//			bt_info.setClickable(false);
			break;

		}
	}

}
