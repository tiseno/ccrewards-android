package com.tiseno.ccrewards;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

class FormInformation {
	public String name;
	public String email;
	public String age;
	public String gender;
	public String incomerange;
	public boolean subscript;
	public String devicetype;
	public String deviceid;

	public FormInformation() {
		this.name = "";
		this.email = "";
		this.age = "0";
		this.gender = "Male";
		this.incomerange = "";
		this.subscript = true;
		this.devicetype = "Android";
		this.deviceid = "";
	}

	public List<BasicNameValuePair> getDataNameValuePair() {
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("gender", gender));
		params.add(new BasicNameValuePair("age", age));
		params.add(new BasicNameValuePair("incomerange", incomerange));
		params.add(new BasicNameValuePair("receivepromotion", subscript ? "1"
				: "0"));
		params.add(new BasicNameValuePair("devicetype", devicetype));
		params.add(new BasicNameValuePair("deviceid", deviceid));
		return params;
	}
}