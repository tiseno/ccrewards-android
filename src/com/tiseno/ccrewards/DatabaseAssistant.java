package com.tiseno.ccrewards;

import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseAssistant {
	private String merchant_table = DBConnection.MERCHANT,
			bank_table = DBConnection.BANK, card_table = DBConnection.CARD,
			favourite_table = DBConnection.FAVOURITE;

	private SQLiteDatabase mDb;

	public DatabaseAssistant(SQLiteDatabase db) {
		mDb = db;

	}

	public Cursor getAllCard() {
		return mDb.query(card_table, null, null, null, null, null, null);
	}

	public Cursor getAllFavourite() {
		return mDb.query(favourite_table, null, null, null, null, null, null);
	}

	public boolean getFavouriteByID(int id) {
		String select = "merchantid = '" + id + "'";
		Cursor c = mDb.query(favourite_table, null, select, null, null, null,
				null);

		if (c.moveToFirst())
			return true;
		return false;
	}

	public boolean getCardByID(int id) {
		String select = "cardid = '" + id + "'";
		Cursor c = mDb.query(card_table, null, select, null, null, null, null);

		if (c.moveToFirst())
			return true;
		return false;
	}

	public boolean insertCard(ContentValues cv) {
		long result;
		try {
			result = mDb.insertOrThrow(card_table, "", cv);
		} catch (SQLiteConstraintException e) {
			e.printStackTrace();
			updateCard(cv, cv.getAsInteger("cardid"));
			result = 0;
		}
		return result > 0;
	}

	public float updateCard(ContentValues cv, int id) {
		float result;
		String[] parms = new String[] { id + "" };
		result = mDb.update(card_table, cv, "cardid=" + id, null);

		return result;
	}

	public boolean insertFavourite(ContentValues cv) {
		long result;
		try {
			result = mDb.insertOrThrow(favourite_table, "", cv);
		} catch (SQLiteConstraintException e) {
			e.printStackTrace();
			updateFavourite(cv, cv.getAsInteger("merchantid"));
			result = 0;
		}
		return result > 0;
	}

	public float updateFavourite(ContentValues cv, int id) {
		float result;
		String[] parms = new String[] { id + "" };
		result = mDb.update(card_table, cv, "merchantid=" + id, null);

		return result;
	}

	public void removeFavourite(int id) {
		try {
			mDb.execSQL("DELETE FROM " + favourite_table + " where merchantid="
					+ id);
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}

	public void removeFavourites(List<Integer> ids) {
		try {
			StringBuilder str = new StringBuilder();
			for (int id : ids) {
				str.append(id + ",");
			}

			if (str.length() > 0)
				str.deleteCharAt(str.length() - 1);

			mDb.execSQL("DELETE FROM " + favourite_table
					+ " where merchantid in(" + str.toString() + ")");
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}

	public void removeMyCards(List<Integer> ids) {
		try {
			StringBuilder str = new StringBuilder();
			for (int id : ids) {
				str.append(id + ",");
			}

			if (str.length() > 0)
				str.deleteCharAt(str.length() - 1);

			mDb.execSQL("DELETE FROM " + card_table + " where cardid in("
					+ str.toString() + ")");
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}

	public void removeMyCard(int id) {
		try {
			mDb.execSQL("DELETE FROM " + card_table + " where cardid=" + id);
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}

}
