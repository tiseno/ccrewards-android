package com.tiseno.ccrewards;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.tiseno.ccrewards.R;

public class InfoActivity extends SherlockActivity {
	TextView tv_info;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
		Global.setActionBar(this, getResources().getString(R.string.info));

		initView();
	}

	private void initView() {
		tv_info = (TextView) findViewById(R.id.act_tv_info);
		tv_info.setText(R.string.info_desc);

		ButtonListerner bl = new ButtonListerner(this);
		bl.setListeners();
		bl.setActiveButton(bl.INFO);
	}

	// back press exit
	boolean doubleBackToExitPressedOnce = false;

	@Override
	public void onBackPressed() {
//		if (doubleBackToExitPressedOnce) {
		Intent intent = new Intent(this, HotDealActivity.class);
		this.startActivity(intent);
		this.finish();
//		super.onBackPressed();
		return;
//		}
//		this.doubleBackToExitPressedOnce = true;
//		Toast.makeText(this,
//				getResources().getString(R.string.press_back_alert),
//				Toast.LENGTH_SHORT).show();
//		new Handler().postDelayed(new Runnable() {
//
//			@Override
//			public void run() {
//				doubleBackToExitPressedOnce = false;
//
//			}
//		}, 2000);
	}
}
