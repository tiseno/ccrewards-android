package com.tiseno.ccrewards;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class NotificationDialog extends Activity implements
		DialogInterface.OnCancelListener {

	private String message;
	private int merchantid;
	private Bundle bundle;
	private AlertDialog alert;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bundle = this.getIntent().getExtras();
		merchantid = bundle.getInt("merchantid",0);
		message = bundle.getString("message");
		Log.i(Global.TAG, "merchantid2: "+ merchantid );
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getResources().getString(R.string.push_dialog_title))
				.setIcon(R.drawable.ic_launcher)
				.setMessage(message)
				.setCancelable(false)
				.setPositiveButton(getResources().getString(R.string.more),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Intent intent = new Intent(
										NotificationDialog.this,
										HotdealDetailActivity.class);
								intent.putExtra("merchantid", merchantid);
								startActivity(intent);
								finish();
							}
						})
				.setNegativeButton(getResources().getString(R.string.close),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								finish();
							}
						});
		alert = builder.create();
		alert.show();
	}

	public void onCancel(DialogInterface dialog) {
		finish();
	}
}
