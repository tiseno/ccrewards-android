package com.tiseno.ccrewards.webservicedata;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.ksoap2.serialization.SoapObject;

import android.os.AsyncTask;
import android.util.Log;

public class WebServiceParsingTask extends
		AsyncTask<String, String, SoapObject> {
	private AsyncTaskCompleteListener<SoapObject> callback;
	private String wsdl_url = "";
	private String namespace = "";
	private String method = "";
	String soapAction = "";
	List<BasicNameValuePair> propety;

	public WebServiceParsingTask() {
		this.wsdl_url = "";
		this.namespace = "";
		this.method = "";
		this.soapAction = "";
		this.propety = new ArrayList<BasicNameValuePair>();
	}

	public WebServiceParsingTask(String wsdl_url, String namespace,
			String method, List<BasicNameValuePair> propety, String soapAction,
			AsyncTaskCompleteListener<SoapObject> callback) {
		this.wsdl_url = wsdl_url;
		this.namespace = namespace;
		this.method = method;
		this.soapAction = soapAction;
		this.propety = (propety == null) ? new ArrayList<BasicNameValuePair>()
				: propety;
		this.callback = callback;
	}

	@Override
	protected SoapObject doInBackground(String... params) {
		WebService ws = new WebService(wsdl_url, namespace, method);
		SoapObject result = ws.CallService(propety, soapAction);
		return result;
	}

	protected void onPostExecute(SoapObject result) {
		callback.onTaskComplete(result);
	}
}
