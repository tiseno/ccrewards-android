package com.tiseno.ccrewards.webservicedata;

public interface AsyncTaskCompleteListener<T> {
	public void onTaskComplete(T result);
}
