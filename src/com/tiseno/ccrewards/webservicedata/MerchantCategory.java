package com.tiseno.ccrewards.webservicedata;

import java.io.Serializable;

public class MerchantCategory implements Serializable {

	public int merchantcategoryid;
	public String merchantcategoryname;
	public String merchantcategoryimage;
}
