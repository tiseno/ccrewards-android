package com.tiseno.ccrewards.webservicedata;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import com.tiseno.ccrewards.Global;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class ListViewImageLoaderTask extends
		AsyncTask<HashMap<String, Object>, Void, HashMap<String, Object>> {
	Context context;
	ListView listview;
	static int count = 0;
	int id = 0;

	int BUFFER_SIZE = 1024 * 23;

	public ListViewImageLoaderTask(Context context, ListView listview) {
		this.context = context;
		this.listview = listview;
		id = count;
		count++;
	}

	@Override
	protected HashMap<String, Object> doInBackground(
			HashMap<String, Object>... hm) {

		InputStream iStream = null;
		String imgUrl = (String) hm[0].get("teaser_path");
		int position = (Integer) hm[0].get("position");

		URL url;
		try {
			url = new URL(imgUrl);
			// Creating an http connection to communicate with url
			HttpURLConnection urlConnection = (HttpURLConnection) url
					.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedInputStream bis = new BufferedInputStream(
					urlConnection.getInputStream(), BUFFER_SIZE);

			// Getting Caching directory
			File cacheDirectory = context.getCacheDir();

			// Temporary file to store the downloaded image
			File tmpFile = new File(cacheDirectory.getPath() + "/ccrewards_"
					+ position + ".png");

			// The FileOutputStream to the temporary file
			FileOutputStream fOutStream = new FileOutputStream(tmpFile);

			// Creating a bitmap from the downloaded inputstream
			Bitmap b = BitmapFactory.decodeStream(bis);

			// Writing the bitmap to the temporary file as png file
			b.compress(Bitmap.CompressFormat.PNG, 80, fOutStream);

			// Flush the FileOutputStream
			fOutStream.flush();

			// Close the FileOutputStream
			fOutStream.close();

			// Create a hashmap object to store image path and its position in
			// the listview
			HashMap<String, Object> hmBitmap = new HashMap<String, Object>();

			// Storing the path to the temporary image file
			hmBitmap.put("image", tmpFile.getPath());

			// Storing the position of the image in the listview
			hmBitmap.put("position", position);

			// Returning the HashMap object containing the image path and
			// position
			return hmBitmap;

		} catch (Exception e) {
			Log.i(Global.TAG, "ecep" + id);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(HashMap<String, Object> result) {
		// Log.i(Global.TAG, "download result " + (result == null) );
		if (result != null) {
			// Getting the path to the downloaded image
			String path = (String) result.get("image");

			// Getting the position of the downloaded image
			int position = (Integer) result.get("position");

			// Getting adapter of the listview
			SimpleAdapter adapter = (SimpleAdapter) listview.getAdapter();

			// Getting the hashmap object at the specified position of the
			// listview
			try {
				HashMap<String, Object> hm = (HashMap<String, Object>) adapter
						.getItem(position);

				// Overwriting the existing path in the adapter
				hm.put("image", path);

				// Noticing listview about the dataset changes
				adapter.notifyDataSetChanged();
			} catch (IndexOutOfBoundsException ex) {
				ex.printStackTrace();
			}
		}
	}
}