package com.tiseno.ccrewards.webservicedata;

import java.io.Serializable;

public class MerchantHotDeal implements Serializable {

	public int merchantid = 0;
	public int merchantcardid﻿ = 0;
	public String merchantname﻿ = "";
	public String merchantdesc﻿ = "";
	public String merchantpromowords = "";
	public String merchanttel = "";
	public String merchantemail = "";
	public String merchanturl = "";
	public String merchantaddress = "";
	public String merchantstate = "";
	public String merchantbankname = "";
	public String merchantteaser﻿﻿﻿ = "";
	public String merchantdate = "";
	public String merchantimage﻿ = "";
	public String merchantcardname = "";

	public String toString() {
		return merchantid + " " + merchantcardid﻿ + " " + merchantname﻿ + " "
				+ merchantdesc﻿ + " " + merchantbankname + " "
				+ merchantteaser﻿﻿﻿ + " " + merchantimage﻿ + " " + merchantaddress + " " 
				+ merchantcardname;
	}
	
	public void setClear() {
		merchantid = 0;
		merchantcardid﻿ = 0;
		merchantname﻿ = "";
		merchantdesc﻿ = "";
		merchantpromowords = "";
		merchanttel = "";
		merchantemail = "";
		merchanturl = "";
		merchantaddress = "";
		merchantstate = "";
		merchantbankname = "";
		merchantteaser﻿﻿﻿ = "";
		merchantdate = "";
		merchantimage﻿ = "";
		merchantcardname = "";
	}
}