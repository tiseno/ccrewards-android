package com.tiseno.ccrewards.webservicedata;

import java.io.Serializable;

public class BankDetail implements Serializable {

	public int bankid﻿;
	public String bankname﻿;
	public String bankimage﻿;
	public int banksort﻿;
	public String bankact;

	public String toString() {
		return bankid﻿ + " " + bankname﻿ + " " + bankimage﻿ + " " + banksort﻿
				+ " " + bankact + " ";
	}

}
