package com.tiseno.ccrewards.webservicedata;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;

import android.util.Log;

/**
 * Ksoap2 for android - output parser This class parses an input soap message
 * 
 * @author tamas.beres@helloandroid com, akos.birtha@helloandroid com
 * 
 */
public class SoapResultParser {

	/**
	 * Parses a single business object containing primitive types from the
	 * response
	 * 
	 * @param input
	 *            soap message, one element at a time
	 * @param theClass
	 *            your class object, that contains the same member names and
	 *            types for the response soap object
	 * @return the values parsed
	 * @throws NumberFormatException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public static void parseDataObject(String input, Object output)
			throws NumberFormatException, IllegalArgumentException,
			IllegalAccessException, InstantiationException {

		Class theClass = output.getClass();
		Field[] fields = theClass.getDeclaredFields();

		for (int i = 0; i < fields.length; i++) {
			Type type = fields[i].getType();
			fields[i].setAccessible(true);

			// detect String
			if (fields[i].getType().equals(String.class)) {
				String tag = fields[i].getName().toString() + "=";

				//replace all speacial char,make tag become plan text 
				tag = tag.replaceAll("\\W", "");
				tag += "=";

				if (input.contains(tag)) {
					String strValue = input.substring(
							input.indexOf(tag) + tag.length(),
							input.indexOf(";", input.indexOf(tag)));
					if (strValue.length() != 0) {
						strValue = strValue.equals("anyType{}") ? "" : strValue;
						fields[i].set(output, strValue);
						// Log.i("TEST", tag + ": " + strValue);
					}
				}
			}

			// detect int or Integer
			if (type.equals(Integer.TYPE) || type.equals(Integer.class)) {
				String tag = fields[i].getName() + "=";

				//replace all speacial char,make tag become plan text 
				tag = tag.replaceAll("\\W", "");
				tag += "=";

				if (input.contains(tag)) {
					String strValue = input.substring(
							input.indexOf(tag) + tag.length(),
							input.indexOf(";", input.indexOf(tag)));
					if (strValue.length() != 0) {
						fields[i].setInt(output, Integer.valueOf(strValue));
//						Log.i("TEST", tag + ": " + strValue);
					}
				}
			}

			// detect float or Float
			if (type.equals(Float.TYPE) || type.equals(Float.class)) {
				String tag = fields[i].getName() + "=";
				
				//replace all speacial char,make tag become plan text 
				tag = tag.replaceAll("\\W", "");
				tag += "=";
				
				if (input.contains(tag)) {
					String strValue = input.substring(
							input.indexOf(tag) + tag.length(),
							input.indexOf(";", input.indexOf(tag)));
					if (strValue.length() != 0) {
						fields[i].setFloat(output, Float.valueOf(strValue));
					}
				}
			}
		}

	}
}