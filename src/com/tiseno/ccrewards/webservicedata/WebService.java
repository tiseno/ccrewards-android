package com.tiseno.ccrewards.webservicedata;

import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.tiseno.ccrewards.Global;

import android.util.Log;

public class WebService {
	private String wsdl_url = "";
	private String namespace = "";
	private String method = "";

	public WebService(String wsdl_url, String namespace, String method) {
		this.wsdl_url = wsdl_url;
		this.namespace = namespace;
		this.method = method;
	}

	public SoapObject CallService(List<BasicNameValuePair> propety,
			String soapAction) {
		SoapObject request = new SoapObject(namespace, method);

		// Use this to add parameters
		for (NameValuePair nvp : propety) {
			request.addProperty(nvp.getName()+"", nvp.getValue()+"");
		}

		// Declare the version of the SOAP request
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		
		try {
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wsdl_url);

			// this is the actual part that will call the webservice
			androidHttpTransport.call(soapAction, envelope);

			// Get the SoapResult from the envelope body.
			SoapObject result = (SoapObject) envelope.getResponse();

			if (result != null) {
				return result;
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
