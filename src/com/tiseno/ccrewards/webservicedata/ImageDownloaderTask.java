package com.tiseno.ccrewards.webservicedata;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.tiseno.ccrewards.Global;
import com.tiseno.ccrewards.R;

public class ImageDownloaderTask extends AsyncTask<String, String, Bitmap> {
	int BUFFER_SIZE = 1024 * 23;

	String imgUrl;
	ImageView imageView;
	Context context;

	public ImageDownloaderTask(Context context, ImageView imageview,
			String imgUrl) {
		this.imgUrl = imgUrl;
		this.imageView = imageview;
		this.context = context;
	}

	@Override
	protected Bitmap doInBackground(String... params) {

		URL url;
		InputStream iStream = null;

		try {
			url = new URL(imgUrl);
			// Creating an http connection to communicate with url
			HttpURLConnection urlConnection = (HttpURLConnection) url
					.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedInputStream bis = new BufferedInputStream(
					urlConnection.getInputStream(), BUFFER_SIZE);

			// Getting Caching directory
			File cacheDirectory = context.getCacheDir();

			// Temporary file to store the downloaded image
			File tmpFile = new File(cacheDirectory.getPath() + "/ccrewards_"
					+ "hotdealDetail" + ".png");

			// The FileOutputStream to the temporary file
			FileOutputStream fOutStream = new FileOutputStream(tmpFile);

			// Creating a bitmap from the downloaded inputstream
			Bitmap b = BitmapFactory.decodeStream(bis);

			// Writing the bitmap to the temporary file as png file
			b.compress(Bitmap.CompressFormat.PNG, 80, fOutStream);

			// Flush the FileOutputStream
			fOutStream.flush();

			// Close the FileOutputStream
			fOutStream.close();

			return b;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Bitmap result) {
	try{	
		if (result != null) {
			Log.i(Global.TAG, "bitmap");
			imageView.setImageBitmap(result);
		} else {
			Log.i(Global.TAG, "bitmap is null");
			imageView.setImageDrawable(context.getResources().getDrawable(
					R.drawable.icon));
		}
	}catch(OutOfMemoryError ex){
		ex.printStackTrace();
	}
		
	}
}
