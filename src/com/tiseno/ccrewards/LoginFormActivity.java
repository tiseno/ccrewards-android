package com.tiseno.ccrewards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ksoap2.serialization.SoapObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.actionbarsherlock.app.SherlockActivity;
import com.andreabaccega.widget.FormEditText;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.WebServiceParsingTask;

public class LoginFormActivity extends SherlockActivity implements
		AsyncTaskCompleteListener<SoapObject> {
	FormEditText et_name, et_email;
	Spinner sp_gender, sp_incomerange, sp_age;
	Button bt_submit;
	RadioGroup rg_subscript;
	RadioButton rb_yes, rb_no;

	Global global;
	Context context;

	FormInformation userInfo = new FormInformation();

	String[] gender;
	String[] incomerange;
	String[] agerange;

	ProgressDialog dialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_form);
		global = new Global(this);
		this.context = this;

		Global.setActionBar(this,
				getResources().getString(R.string.firstTimeLogin));
		initView();
	}

	private void initView() {
		et_name = (FormEditText) findViewById(R.id.act_form_et_name);
		et_name.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					et_name.testValidity();
				}
			}
		});
		
		et_email = (FormEditText) findViewById(R.id.act_form_et_email);
		et_email.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					et_email.testValidity();
				}
			}
		});

		rg_subscript = (RadioGroup) findViewById(R.id.act_form_rg_received);
		rg_subscript.setOnCheckedChangeListener(SubscriptListerner);

		rb_yes = (RadioButton) findViewById(R.id.act_form_rb_yes);
		rb_no = (RadioButton) findViewById(R.id.act_form_rb_no);

		bt_submit = (Button) findViewById(R.id.act_form_bt_submit);
		bt_submit.setOnClickListener(SubmitListerner);

		// init spinner
		sp_gender = (Spinner) findViewById(R.id.act_form_sp_gender);
		gender = getResources().getStringArray(R.array.gender);
		initSPinner("gender", sp_gender, gender);

		sp_gender.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				userInfo.gender = gender[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		sp_incomerange = (Spinner) findViewById(R.id.act_form_sp_interest);
		incomerange = getResources().getStringArray(R.array.incomerange);
		initSPinner("incomerange", sp_incomerange, incomerange);

		sp_incomerange.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				userInfo.incomerange = incomerange[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		sp_age = (Spinner) findViewById(R.id.act_form_sp_age);
		agerange = getResources().getStringArray(R.array.agerange);
		initSPinner("age", sp_age, agerange);

		sp_age.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				userInfo.age = agerange[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

	}

	private void initSPinner(String key, Spinner spinner, String[] dataArray) {
		final List<Map<String, String>> data = new ArrayList<Map<String, String>>();
		for (int i = 0; i < dataArray.length; i++) {
			data.add(addData(key, dataArray[i]));
		}

		SimpleAdapter adapter = new SimpleAdapter(this, data,
				android.R.layout.simple_spinner_item, new String[] { key },
				new int[] { android.R.id.text1 });
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
	}

	private Map<String, String> addData(String key, String value) {
		Map<String, String> map = new HashMap<String, String>();
		map.put(key, value);
		return map;
	}

	private RadioGroup.OnCheckedChangeListener SubscriptListerner = new RadioGroup.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			if (checkedId == rb_yes.getId()) {
				userInfo.subscript = true;
			} else if (checkedId == rb_no.getId()) {
				userInfo.subscript = false;
			}
		}
	};

	private View.OnClickListener SubmitListerner = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			if (Global.haveNetworkConnection(LoginFormActivity.this)) {
				if (et_name.testValidity() && et_email.testValidity()) {
					userInfo.name = et_name.getText().toString();
					userInfo.email = et_email.getText().toString();
					// userInfo.age = et_age.getText().toString();
					userInfo.devicetype = global.getDeviceName();
					userInfo.deviceid = global.getDeviceID(context);

					WebServiceParsingTask webTask = new WebServiceParsingTask(
							Global.WSDL_URL,
							Global.NAMESPACE,
							Global.SOAPMETHOD_INSERTSURVEY,
							userInfo.getDataNameValuePair(),
							Global.SOAPACTION_INSERTSURVEY,
							(AsyncTaskCompleteListener<SoapObject>) LoginFormActivity.this);
					webTask.execute();

					dialog = ProgressDialog.show(
							LoginFormActivity.this,
							"",
							LoginFormActivity.this.getResources().getString(
									R.string.pass_loginform_loading_dialog),
							true);
				}
			}
		}
	};

	@Override
	public void onTaskComplete(SoapObject result) {
		if (result != null) {
			String str = result.toString();
			String msg = "";
			if (str.contains("surveycreatedate=")) {
				msg = this.getResources().getString(R.string.success);
				progressAfterPassForm();
			} else {
				msg = this.getResources().getString(R.string.progress_warning);
			}

			dialog.setMessage(msg);
			new Handler().postDelayed(new Runnable() {
				public void run() {
					try {
						dialog.dismiss();
						dialog = null;
					} catch (Exception e) {
						// nothing
					}
				}
			}, 2000);
		}
	}

	private void progressAfterPassForm() {
		global.InsertSharePreferences(Global.ALREADY_PASS_SURVEY, true);
		Intent intent = new Intent(this, HotDealActivity.class);
		startActivity(intent);
		finish();
	}
}
