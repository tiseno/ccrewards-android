package com.tiseno.ccrewards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;

import com.actionbarsherlock.app.SherlockActivity;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.ListViewImageLoaderTask;
import com.tiseno.ccrewards.webservicedata.MerchantHotDeal;
import com.tiseno.ccrewards.webservicedata.SoapResultParser;
import com.tiseno.ccrewards.webservicedata.WebServiceParsingTask;

public class HotDealActivity extends SherlockActivity implements
		AsyncTaskCompleteListener<SoapObject> {
	private ListView hotdeal_listview;
	private List<MerchantHotDeal> merchantDetail = new ArrayList<MerchantHotDeal>();
	private List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();

	String[] from = { "title", "bankname", "image" };
	int[] to = { R.id.hotdeal_title_tv, R.id.hotdeal_bankname_tv,
			R.id.hotdeal_rl_image };

	MerchantHotDeal _hotdeal;
	
	ProgressBar dialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hot_deal);
		Global.setActionBar(this, getResources().getString(R.string.hotdeals));
		if (Global.haveNetworkConnection(this))
			initView();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		data.clear();
		this.onCreate(null);
	}

	private void initView() {
		ButtonListerner bl = new ButtonListerner(this);
		bl.setListeners();
		bl.setActiveButton(bl.HOTDEAL);

		hotdeal_listview = (ListView) findViewById(R.id.act_hotdeal_listview);
		hotdeal_listview.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView adapterView, View view,
					int position, long id) {
				SimpleAdapter adapter = (SimpleAdapter) adapterView
						.getAdapter();

				Intent intent = new Intent(HotDealActivity.this,
						HotdealDetailActivity.class);
				intent.putExtra("merchantDetail", merchantDetail.get(position));
				startActivity(intent);

			}
		});
		getHotdeals();
	}

	private void getHotdeals() {
		WebServiceParsingTask webTask = new WebServiceParsingTask(
				Global.WSDL_URL, Global.NAMESPACE, Global.SOAPMETHOD_HOTDEALS,
				null, Global.SOAPACTION_HOTDEALS, this);

		webTask.execute();

		dialog = (ProgressBar) findViewById(R.id.progressBar);
		dialog.setVisibility(View.VISIBLE);
	}

	@Override
	public void onTaskComplete(SoapObject result) {
		if (result != null) {
			for (int index = 0; index < result.getPropertyCount(); index++) {
				SoapObject childObj = (SoapObject) result.getProperty(index);
				_hotdeal = new MerchantHotDeal();
				SoapResultParser soapResult = new SoapResultParser();
				try {
					soapResult.parseDataObject(childObj.toString(), _hotdeal);
					merchantDetail.add(_hotdeal);
					HashMap<String, Object> _data = new HashMap<String, Object>();
					_data.put("title", _hotdeal.merchantpromowords);
					_data.put("bankname", _hotdeal.merchantbankname);
					_data.put("image", android.R.color.transparent);
					_data.put("teaser_path", _hotdeal.merchantimage﻿);

					Log.i(Global.TAG, "merchantteaser﻿﻿﻿ "
							+ _hotdeal.merchantimage﻿);
					data.add(_data);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// dialog.dismiss();
			dialog.setVisibility(View.GONE);
			LoadListView();
		}
	}

	private void LoadListView() {
		SimpleAdapter adapter = new SimpleAdapter(this, data,
				R.layout.hotdeal_card, from, to);
		hotdeal_listview.setAdapter(adapter);

		for (int i = 0; i < adapter.getCount(); i++) {
			HashMap<String, Object> hm = (HashMap<String, Object>) adapter
					.getItem(i);
			String imgUrl = (String) hm.get("teaser_path");

			ListViewImageLoaderTask imageLoaderTask = new ListViewImageLoaderTask(
					this, hotdeal_listview);

			HashMap<String, Object> hmDownload = new HashMap<String, Object>();
			String url = Global.IMAGE_URL + imgUrl;
			hm.put("teaser_path", url);
			hm.put("position", i);

			// Starting ImageLoaderTask to download and populate image in
			// the listview
			imageLoaderTask.execute(hm);
		}
	}

	// back press exit
	boolean doubleBackToExitPressedOnce = false;

	@Override
	public void onBackPressed() {
//		if (doubleBackToExitPressedOnce) {
		new AlertDialog.Builder(this)
		.setTitle("Exit Credit Card Rewards")
		.setMessage("Are you sure you want to exit ?")
		.setNegativeButton(android.R.string.no, null)
		.setPositiveButton(android.R.string.yes, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				HotDealActivity.super.onBackPressed();
			}
		}).create().show();
//			super.onBackPressed();
//			return;
//		}
//		this.doubleBackToExitPressedOnce = true;
//		Toast.makeText(this,
//				getResources().getString(R.string.press_back_alert),
//				Toast.LENGTH_SHORT).show();
//		new Handler().postDelayed(new Runnable() {
//
//			@Override
//			public void run() {
//				doubleBackToExitPressedOnce = false;
//
//			}
//		}, 2000);
	}
}
