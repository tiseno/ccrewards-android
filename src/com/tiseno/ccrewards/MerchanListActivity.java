package com.tiseno.ccrewards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.ksoap2.serialization.SoapObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.ListViewImageLoaderTask;
import com.tiseno.ccrewards.webservicedata.MerchantHotDeal;
import com.tiseno.ccrewards.webservicedata.SoapResultParser;
import com.tiseno.ccrewards.webservicedata.WebServiceParsingTask;

public class MerchanListActivity extends SherlockActivity implements
		AsyncTaskCompleteListener<SoapObject> {
	private List<MerchantHotDeal> merchantDetail = new ArrayList<MerchantHotDeal>();
	private List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
	private Context context;
	private ListView lv_bank;

	private TextView title;

	private ProgressBar dialog;

	private int cardid = 0;
	private int categoryid = 0;
	private String catname = "";

	private Activity activity;

	private List<BasicNameValuePair> propety = new ArrayList<BasicNameValuePair>();

	String[] from = { "title", "promo", "image" };
	int[] to = { R.id.merchanlist_title, R.id.merchanlist_promo,
			R.id.merchanlist_im };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Global.setActionBar(this, getResources().getString(R.string.category));
		setContentView(R.layout.activity_bankcard_listview);
		activity = this;
		context = this;

		try {
			Bundle bn = new Bundle();
			bn = getIntent().getExtras();

			cardid = bn.getInt("cardid");
			categoryid = bn.getInt("categoryid");
			catname = bn.getString("merchantcategoryname﻿");

			propety.add(new BasicNameValuePair("categoryid", categoryid + ""));
			propety.add(new BasicNameValuePair("cardid", cardid + ""));

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (Global.haveNetworkConnection(this))
			initView();
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onTaskComplete(SoapObject result) {
		for (int index = 0; index < result.getPropertyCount(); index++) {
			SoapObject childObj = (SoapObject) result.getProperty(index);
			MerchantHotDeal merchantHotDeal = new MerchantHotDeal();
			try {
				SoapResultParser.parseDataObject(childObj.toString(),
						merchantHotDeal);
				merchantDetail.add(merchantHotDeal);
				HashMap<String, Object> _data = new HashMap<String, Object>();
				_data.put("name", merchantHotDeal.merchantname﻿);
				_data.put("title", merchantHotDeal.merchantname﻿);
				_data.put("promo", merchantHotDeal.merchantpromowords);
				_data.put("image", R.drawable.default_no_image);	// default image here
				_data.put("teaser_path", merchantHotDeal.merchantteaser﻿﻿﻿);
				Log.i(Global.TAG, "childObj " + childObj.toString());
				data.add(_data);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// dialog.dismiss();
		dialog.setVisibility(View.GONE);
		LoadListView();
	}

	private void LoadListView() {
		SimpleAdapter adapter = new SimpleAdapter(this, data,
				R.layout.button_simple_merchan_layout, from, to);
		lv_bank.setAdapter(adapter);

		for (int i = 0; i < adapter.getCount(); i++) {
			HashMap<String, Object> hm = (HashMap<String, Object>) adapter
					.getItem(i);
			String imgUrl = (String) hm.get("teaser_path");

			ListViewImageLoaderTask imageLoaderTask = new ListViewImageLoaderTask(
					this, lv_bank);

			HashMap<String, Object> hmDownload = new HashMap<String, Object>();
			String url = Global.IMAGE_URL + imgUrl;
			hm.put("teaser_path", url);
			hm.put("position", i);

			// Starting ImageLoaderTask to download and populate image in
			// the listview
			imageLoaderTask.execute(hm);
		}
		;
	}

	private void initView() {
		ButtonListerner bl = new ButtonListerner(this);
		bl.setListeners();
		bl.setActiveButton(bl.SEARCH);

		title = (TextView) findViewById(R.id.st_cardlist__tv_title);
		title.setTypeface(Global.getTypeface(context, "Wisdom Script.otf"));
		title.setText(catname + "");

		lv_bank = (ListView) findViewById(R.id.act_search_bankcard_lv);
		lv_bank.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView adapterView, View view,
					int position, long id) {
				SimpleAdapter adapter = (SimpleAdapter) adapterView
						.getAdapter();

				HashMap<String, Object> item = (HashMap<String, Object>) adapter
						.getItem(position);
				MerchantHotDeal _category = merchantDetail.get(position);
				Intent intent = new Intent(activity,
						HotdealDetailActivity.class);
				intent.putExtra("merchantid", _category.merchantid);
				startActivity(intent);
			}
		});

		getAllMerchan();

	}

	private void getAllMerchan() {
		WebServiceParsingTask webTask = new WebServiceParsingTask(
				Global.WSDL_URL, Global.NAMESPACE,
				Global.SOAPMETHOD_GETMERH_BY_CARD_CATID, propety,
				Global.SOAPACTION_GETMERH_BY_CARD_CATID, this);

		webTask.execute();

		// dialog = ProgressDialog.show(this, "",
		// this.getResources().getString(R.string.loading_dialog), true);
		dialog = (ProgressBar) findViewById(R.id.progressBar);
		dialog.setVisibility(View.VISIBLE);
	}

}
