package com.tiseno.ccrewards;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConnection extends SQLiteOpenHelper {
	public static final String DATABASE_NAME = "CreditCardRewards.db";
//	public static final String DATABASE_NAME = "/mnt/sdcard/CreditCardRewards/CreditCardRewards.db";
	public static final int DATABASE_VERSION = 1;
	public static final String MERCHANT = "merchant";
	public static final String BANK = "bank";
	public static final String CARD = "mycard";
	public static final String FAVOURITE = "favourite";

	public DBConnection(Context context, CursorFactory factory) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sqll = "CREATE TABLE 'merchant'("
				+ "'merchantid' INTEGER PRIMARY KEY,"+
				"'merchantname' varchar(255) NOT NULL ," + 
				"'merchanttel' varchar(255) NOT NULL ," + 
				"'merchantemail' varchar(255) NOT NULL ," +
				"'merchanturl' varchar(255) NOT NULL ," +
				"'merchantaddress' varchar(255) NOT NULL ," +
				"'merchantstate' varchar(255) NOT NULL ," +
				"'merchantdesc' varchar(255) NOT NULL ," +
				"'merchantsort' int(1) NOT NULL DEFAULT '0' ," + 
				"'merchantact' varchar(255) NOT NULL ," +
				"'merchantteaser' varchar(255) NOT NULL ," +
				"'merchantimage' varchar(255) NOT NULL ," +
				"'merchantdate' varchar(255) NOT NULL ," +
				"'merchantcategoryid' int(1) NOT NULL DEFAULT '0' ," + 
				"'merchantlan' varchar(255) NOT NULL ," +
				"'merchantlong' varchar(255) NOT NULL ," +
				"'merchantcategoryname' varchar(255) NOT NULL ," +
				"'merchantcardname' varchar(255) NOT NULL ," +
				"'merchantbankname' varchar(255) NOT NULL" +
				");";
		
		db.execSQL(sqll);
		
		sqll = "CREATE TABLE 'bank'("
				+ "'bankid' INTEGER PRIMARY KEY,"+
				"'bankname' varchar(255) NOT NULL ," + 
				"'bankimage' varchar(255) NOT NULL ," + 
				"'banksort' int(1) NOT NULL DEFAULT '0' ," + 
				"'bankact' varchar(255) NOT NULL " +
				");";
		
		db.execSQL(sqll);
		
		sqll = "CREATE TABLE 'mycard'("+
				"'cardid' INTEGER PRIMARY KEY"+
				");";
		
		db.execSQL(sqll);
		
		sqll = "CREATE TABLE 'favourite'("+
				"'merchantid' INTEGER PRIMARY KEY"+
				");";
		
		db.execSQL(sqll);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}

}
