package com.tiseno.ccrewards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.ksoap2.serialization.SoapObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.BankCardDetail;
import com.tiseno.ccrewards.webservicedata.ListViewImageLoaderTask;
import com.tiseno.ccrewards.webservicedata.SoapResultParser;
import com.tiseno.ccrewards.webservicedata.WebServiceParsingTask;

public class SearchBankCardActivity extends SherlockActivity implements
		AsyncTaskCompleteListener<SoapObject> {
	private List<BankCardDetail> banksData = new ArrayList<BankCardDetail>();
	private List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();

	private ListView lv_bank;

	private TextView title;

	private ProgressBar dialog;

	private int bankid = 0;
	private String bankname = "";

	private List<BasicNameValuePair> propety = new ArrayList<BasicNameValuePair>();

	String[] from = { "name", "image" };
	int[] to = { R.id.cardlist_tv, R.id.cardlist_im };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Global.setActionBar(this, getResources().getString(R.string.card));
		setContentView(R.layout.activity_bankcard_listview);
		try {
			Bundle bn = new Bundle();
			bn = getIntent().getExtras();

			bankid = bn.getInt("bankid");
			bankname = bn.getString("bankname");

			propety.add(new BasicNameValuePair("bankid", bankid + ""));

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (Global.haveNetworkConnection(this))
			initView();
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onTaskComplete(SoapObject result) {
		for (int index = 0; index < result.getPropertyCount(); index++) {
			SoapObject childObj = (SoapObject) result.getProperty(index);
			BankCardDetail _bankDetail = new BankCardDetail();
			SoapResultParser soapResult = new SoapResultParser();
			try {
				soapResult.parseDataObject(childObj.toString(), _bankDetail);
				banksData.add(_bankDetail);
				HashMap<String, Object> _data = new HashMap<String, Object>();
				_data.put("name", _bankDetail.cardname﻿);
				_data.put("image", R.drawable.ic_nav_cards_active);		// default image here
				_data.put("teaser_path", _bankDetail.cardimage﻿﻿﻿﻿﻿);

				Log.i(Global.TAG, "childObj " + childObj.toString());
				data.add(_data);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// dialog.dismiss();
		dialog.setVisibility(View.GONE);
		LoadListView();
	}

	private void LoadListView() {
		SimpleAdapter adapter = new SimpleAdapter(this, data,
				R.layout.button_bankcard_layout, from, to);
		lv_bank.setAdapter(adapter);
		
		for (int i = 0; i < adapter.getCount(); i++) {
			HashMap<String, Object> hm = (HashMap<String, Object>) adapter
					.getItem(i);
			String imgUrl = (String) hm.get("teaser_path");

			ListViewImageLoaderTask imageLoaderTask = new ListViewImageLoaderTask(
					this, lv_bank);

			HashMap<String, Object> hmDownload = new HashMap<String, Object>();
			String url = String.format(Global.IMAGE_URL+"%s", Uri.encode(imgUrl));
			hm.put("teaser_path", url);
			hm.put("position", i);

			// Starting ImageLoaderTask to download and populate image in
			// the listview
			imageLoaderTask.execute(hm);
			Log.i("bankcard ---------------------------------------", imgUrl);
		}
		
	}

	private void initView() {
		ButtonListerner bl = new ButtonListerner(this);
		bl.setListeners();
		bl.setActiveButton(bl.SEARCH);

		title = (TextView) findViewById(R.id.st_cardlist__tv_title);
		title.setText(bankname + "");

		lv_bank = (ListView) findViewById(R.id.act_search_bankcard_lv);
		lv_bank.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView adapterView, View view,
					int position, long id) {
				SimpleAdapter adapter = (SimpleAdapter) adapterView
						.getAdapter();

				HashMap<String, Object> item = (HashMap<String, Object>) adapter
						.getItem(position);
				
				BankCardDetail bd = banksData.get(position);
				Intent intent = new Intent(SearchBankCardActivity.this,
						SearchMerchanCatActivity.class);
				intent.putExtra("cardid", bd.cardid﻿);
				intent.putExtra("cardname", bd.cardname﻿);
				startActivity(intent);
			}
		});

		getAllCard();

	}

	private void getAllCard() {
		WebServiceParsingTask webTask = new WebServiceParsingTask(
				Global.WSDL_URL, Global.NAMESPACE,
				Global.SOAPMETHOD_GETCARDBYID, propety,
				Global.SOAPACTION_GETCARDBYID, this);

		webTask.execute();

		// dialog = ProgressDialog.show(this, "",
		// this.getResources().getString(R.string.loading_dialog), true);
		dialog = (ProgressBar) findViewById(R.id.progressBar);
		dialog.setVisibility(View.VISIBLE);
	}

}
