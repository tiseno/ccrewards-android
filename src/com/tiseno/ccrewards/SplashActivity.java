package com.tiseno.ccrewards;

import static com.tiseno.ccrewards.CommonUtilities.SENDER_ID;
import static com.tiseno.ccrewards.CommonUtilities.TAG;

import java.util.HashMap;

import org.ksoap2.serialization.SoapObject;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.google.android.gcm.GCMRegistrar;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.MerchantHotDeal;
import com.tiseno.ccrewards.webservicedata.SoapResultParser;

public class SplashActivity extends Activity implements
		AsyncTaskCompleteListener<SoapObject> {
	private final int SPLASH_DISPLAY_LENGTH = 2000;
	private Global global;

	private SQLiteDatabase db;
	static DatabaseAssistant da;
	private DBConnection helper = new DBConnection(this, null);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);

		db = helper.getWritableDatabase();
		da = new DatabaseAssistant(db);

		global = new Global(this);
		Log.i(Global.TAG, "density: "+getApplicationContext().getResources()
				.getDisplayMetrics().density + "");
//		regGCMservice();
	}

	private void regGCMservice() {
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		String regId = GCMRegistrar.getRegistrationId(this);
		if (regId.equals("")) {
			GCMRegistrar.register(this, SENDER_ID);
		} else {
			Log.v(TAG, "Already registered");
			Global.insertToken(this, regId, this);
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				boolean result = global.GetSharePreferences(
						Global.ALREADY_PASS_SURVEY, false);

				if (result) {
					Intent mainIntent = new Intent(SplashActivity.this,
							HotDealActivity.class);
					SplashActivity.this.startActivity(mainIntent);
					SplashActivity.this.finish();
				} else {
					Intent mainIntent = new Intent(SplashActivity.this,
							LoginFormActivity.class);
					SplashActivity.this.startActivity(mainIntent);
					SplashActivity.this.finish();
				}
			}
		}, SPLASH_DISPLAY_LENGTH);
	}

	@Override
	public void onTaskComplete(SoapObject result) {
		// Log.i("TEST", result.toString());
	}
}