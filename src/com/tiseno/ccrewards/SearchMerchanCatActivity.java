package com.tiseno.ccrewards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.ksoap2.serialization.SoapObject;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.ListViewImageLoaderTask;
import com.tiseno.ccrewards.webservicedata.MerchantCategory;
import com.tiseno.ccrewards.webservicedata.SoapResultParser;
import com.tiseno.ccrewards.webservicedata.WebServiceParsingTask;

public class SearchMerchanCatActivity extends SherlockActivity implements
		AsyncTaskCompleteListener<SoapObject> {
	private List<MerchantCategory> catsData = new ArrayList<MerchantCategory>();
	private List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
	private Context context;

	private ListView lv_bank;

	private TextView title;

	private ProgressBar dialog;

	private int cardid = 0;
	private String cardname = "";

	private Button bt_addcard;

	private List<BasicNameValuePair> propety = new ArrayList<BasicNameValuePair>();

	private SQLiteDatabase db;
	static DatabaseAssistant da;
	private DBConnection helper = new DBConnection(this, null);

	String[] from = { "name", "image" };
	int[] to = { R.id.cardlist_tv, R.id.cardlist_im };

	private boolean isAdd = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Global.setActionBar(this, getResources().getString(R.string.category));
		setContentView(R.layout.activity_bankcard_listview);
		this.context = this;
		db = helper.getWritableDatabase();
		da = new DatabaseAssistant(db);

		try {
			Bundle bn = new Bundle();
			bn = getIntent().getExtras();

			cardid = bn.getInt("cardid");
			cardname = bn.getString("cardname");

			propety.add(new BasicNameValuePair("cardid", cardid + ""));

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (Global.haveNetworkConnection(this)) {
			initView();
			checkButtonStatus();
		}
		super.onCreate(savedInstanceState);
	}

	private void checkButtonStatus() {
		isAdd = da.getCardByID(cardid);
		if (isAdd) {
			bt_addcard.setBackgroundDrawable(context.getResources()
					.getDrawable(R.drawable.btn_favourites_active));
		} else {
			bt_addcard.setBackgroundDrawable(context.getResources()
					.getDrawable(R.drawable.bt_addfavorites));
		}
	}

	@Override
	public void onTaskComplete(SoapObject result) {
		for (int index = 0; index < result.getPropertyCount(); index++) {
			SoapObject childObj = (SoapObject) result.getProperty(index);
			MerchantCategory _bankDetail = new MerchantCategory();
			SoapResultParser soapResult = new SoapResultParser();
			try {
				soapResult.parseDataObject(childObj.toString(), _bankDetail);
				catsData.add(_bankDetail);
				HashMap<String, Object> _data = new HashMap<String, Object>();
				_data.put("name", _bankDetail.merchantcategoryname);
				_data.put("image", R.drawable.default_no_image);	// default image here
				_data.put("teaser_path", _bankDetail.merchantcategoryimage);

				Log.i(Global.TAG, "childObj " + childObj.toString());
				data.add(_data);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// dialog.dismiss();
		dialog.setVisibility(View.GONE);
		LoadListView();
	}

	private void LoadListView() {
		SimpleAdapter adapter = new SimpleAdapter(this, data,
				R.layout.button_bankcard_layout, from, to);
		lv_bank.setAdapter(adapter);
		
		for (int i = 0; i < adapter.getCount(); i++) {
			HashMap<String, Object> hm = (HashMap<String, Object>) adapter
					.getItem(i);
			String imgUrl = (String) hm.get("teaser_path");

			ListViewImageLoaderTask imageLoaderTask = new ListViewImageLoaderTask(
					this, lv_bank);

			HashMap<String, Object> hmDownload = new HashMap<String, Object>();
			String url = Global.IMAGE_URL + imgUrl;
			hm.put("teaser_path", url);
			hm.put("position", i);

			// Starting ImageLoaderTask to download and populate image in
			// the listview
			imageLoaderTask.execute(hm);
		}
		;
	}

	private void initView() {
		ButtonListerner bl = new ButtonListerner(this);
		bl.setListeners();
		bl.setActiveButton(bl.SEARCH);

		title = (TextView) findViewById(R.id.st_cardlist__tv_title);
		title.setText(cardname + "");

		bt_addcard = (Button) findViewById(R.id.st_cardlist_bt_addcard);
		bt_addcard.setVisibility(View.VISIBLE);
		bt_addcard.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (da != null) {
					if (!isAdd) {
						ContentValues cv = new ContentValues();
						cv.put("cardid", cardid);
						if (da.insertCard(cv)) {
							Toast.makeText(
									context,
									context.getResources().getString(
											R.string.add2mycard), 1500).show();
						}
					} else {
						da.removeMyCard(cardid);
					}
				} else
					Log.i(Global.TAG, "da is null");
				checkButtonStatus();

			}
		});

		lv_bank = (ListView) findViewById(R.id.act_search_bankcard_lv);
		lv_bank.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView adapterView, View view,
					int position, long id) {
				SimpleAdapter adapter = (SimpleAdapter) adapterView
						.getAdapter();

				HashMap<String, Object> item = (HashMap<String, Object>) adapter
						.getItem(position);
				MerchantCategory _category = catsData.get(position);
				Intent intent = new Intent(SearchMerchanCatActivity.this,
						MerchanListActivity.class);
				intent.putExtra("cardid", cardid);
				intent.putExtra("categoryid", _category.merchantcategoryid);
				intent.putExtra("merchantcategoryname﻿",
						_category.merchantcategoryname);
				startActivity(intent);
			}
		});

		getAllCard();

	}

	private void getAllCard() {
		WebServiceParsingTask webTask = new WebServiceParsingTask(
				Global.WSDL_URL, Global.NAMESPACE,
				Global.SOAPMETHOD_GETMERH_CAT_BY_ID, propety,
				Global.SOAPACTION_GETMERH_CAT_BY_ID, this);

		webTask.execute();

		// dialog = ProgressDialog.show(this, "",
		// this.getResources().getString(R.string.loading_dialog), true);

		dialog = (ProgressBar) findViewById(R.id.progressBar);
		dialog.setVisibility(View.VISIBLE);
	}

}
