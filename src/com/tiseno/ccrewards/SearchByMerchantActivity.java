package com.tiseno.ccrewards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.ksoap2.serialization.SoapObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.ListViewImageLoaderTask;
import com.tiseno.ccrewards.webservicedata.MerchantHotDeal;
import com.tiseno.ccrewards.webservicedata.SoapResultParser;
import com.tiseno.ccrewards.webservicedata.WebServiceParsingTask;

public class SearchByMerchantActivity extends SherlockActivity implements
		AsyncTaskCompleteListener<SoapObject> {
	private List<MerchantHotDeal> merchantDetail = new ArrayList<MerchantHotDeal>();
	private List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();

	private TextView tv_askSearch;

	private ListView lv_bank;

	ProgressBar dialog;

	private Activity activity;

	private boolean TaskRunning = false;

	private List<BasicNameValuePair> propety = new ArrayList<BasicNameValuePair>();

	String[] from = { "title", "promo", "image" };
	int[] to = { R.id.merchanlist_title, R.id.merchanlist_promo,
			R.id.merchanlist_im };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Global.setActionBar(this, getResources().getString(R.string.search));
		setContentView(R.layout.activity_searchbybank);
		activity = this;
		if (Global.haveNetworkConnection(this))
			initView();
		super.onCreate(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		final SearchView searchView = new SearchView(getSupportActionBar()
				.getThemedContext());
		searchView.setQueryHint("Search by Name");
		searchView.setIconified(false);
		searchView.onActionViewCollapsed();
		searchView.setFocusable(true);
		searchView.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextChange(String newText) {
				Log.i("byName (onQueryTextChange) --------------------------", "onQueryTextChange");
				return false;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				if (query.length() < 3) {
					tv_askSearch.setVisibility(View.VISIBLE);
					tv_askSearch.setText(getResources().getString(R.string.min_char));
					return true;
				} else {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
					Log.i(Global.TAG, "Serach text: "+ query);
					propety.clear();
					propety.add(new BasicNameValuePair("keywords", query + ""));
					initListView();
					return false;
				}	
			}
		});

		menu.add("Search")
				.setIcon(R.drawable.ic_search)
				.setActionView(searchView)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_IF_ROOM
								| MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		return true;
	}

	@Override
	public void onTaskComplete(SoapObject result) {
		merchantDetail.clear();
		data.clear();
		TaskRunning = false;
		for (int index = 0; index < result.getPropertyCount(); index++) {
			SoapObject childObj = (SoapObject) result.getProperty(index);
			MerchantHotDeal merchantHotDeal = new MerchantHotDeal();
			try {
				SoapResultParser.parseDataObject(childObj.toString(),
						merchantHotDeal);
				merchantDetail.add(merchantHotDeal);
				HashMap<String, Object> _data = new HashMap<String, Object>();
				_data.put("title", merchantHotDeal.merchantname﻿);
				_data.put("promo", merchantHotDeal.merchantpromowords);
				_data.put("image", R.drawable.default_no_image);	// default image
				_data.put("teaser_path", merchantHotDeal.merchantteaser﻿﻿﻿);
				Log.i(Global.TAG, "childObj " + childObj.toString());
				data.add(_data);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// dialog.dismiss();
		dialog.setVisibility(View.GONE);
		LoadListView();
	}

	private void LoadListView() {
		SimpleAdapter adapter = new SimpleAdapter(this, data,
				R.layout.button_simple_merchan_layout, from, to);
		lv_bank.setAdapter(adapter);

		if (data.isEmpty()) {
			tv_askSearch.setVisibility(View.VISIBLE);
			tv_askSearch.setText(getResources().getString(R.string.no_result));
		} else
			tv_askSearch.setVisibility(View.GONE);

		for (int i = 0; i < adapter.getCount(); i++) {
			HashMap<String, Object> hm = (HashMap<String, Object>) adapter
					.getItem(i);
			String imgUrl = (String) hm.get("teaser_path");

			ListViewImageLoaderTask imageLoaderTask = new ListViewImageLoaderTask(
					this, lv_bank);

			HashMap<String, Object> hmDownload = new HashMap<String, Object>();
			String url = Global.IMAGE_URL + imgUrl;
			hm.put("teaser_path", url);
			hm.put("position", i);

			// Starting ImageLoaderTask to download and populate image in
			// the listview
			imageLoaderTask.execute(hm);
		}
	}

	private void initView() {
		ButtonListerner bl = new ButtonListerner(this);
		bl.setListeners();
		bl.setActiveButton(bl.SEARCH);

		tv_askSearch = (TextView) findViewById(R.id.ask_search_tv);
		tv_askSearch.setVisibility(View.VISIBLE);

		Button bt_searchByBank = (Button) findViewById(R.id.st_search_bank);
		bt_searchByBank.setTypeface(Global.getTypeface(this,
				"Wisdom Script.otf"));
		bt_searchByBank.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SearchByMerchantActivity.this,
						SearchByBankActivity.class);
				startActivity(intent);
				SearchByMerchantActivity.this.finish();
			}
		});

		Button bt_searchByMerchant = (Button) findViewById(R.id.st_search_merchan);
		bt_searchByMerchant.setTypeface(Global.getTypeface(this,
				"Wisdom Script.otf"));
		bt_searchByMerchant.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.button_click));
		bt_searchByMerchant.setClickable(false);
	}

	private void initListView() {
		lv_bank = (ListView) findViewById(R.id.act_search_bank_lv);
		lv_bank.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView adapterView, View view,
					int position, long id) {
				SimpleAdapter adapter = (SimpleAdapter) adapterView
						.getAdapter();

				HashMap<String, Object> item = (HashMap<String, Object>) adapter
						.getItem(position);
				MerchantHotDeal _category = merchantDetail.get(position);
				Intent intent = new Intent(activity,
						HotdealDetailActivity.class);
				intent.putExtra("merchantid", _category.merchantid);
				startActivity(intent);
			}
		});

		getAllMerchan();

	}

	private void getAllMerchan() {
		if (!TaskRunning) {
			TaskRunning = true;
			WebServiceParsingTask webTask = new WebServiceParsingTask(
					Global.WSDL_URL, Global.NAMESPACE,
					Global.SOAPMETHOD_GETMERH_BY_KEYWORD, propety,
					Global.SOAPACTION_GETMERH_BY_KEYWORD, this);

			webTask.execute();

			// dialog = ProgressDialog.show(this, "", this.getResources()
			// .getString(R.string.loading_dialog), true);
			dialog = (ProgressBar) findViewById(R.id.progressBar);
			dialog.setVisibility(View.VISIBLE);
			tv_askSearch.setVisibility(View.GONE);
		}
	}

	// back press exit
	boolean doubleBackToExitPressedOnce = false;

	@Override
	public void onBackPressed() {
		// if (doubleBackToExitPressedOnce) {
		Intent intent = new Intent(this, SearchByBankActivity.class);
		this.startActivity(intent);
		this.finish();
//		super.onBackPressed();
		return;
		// }
		// this.doubleBackToExitPressedOnce = true;
		// Toast.makeText(this,
		// getResources().getString(R.string.press_back_alert),
		// Toast.LENGTH_SHORT).show();
		// new Handler().postDelayed(new Runnable() {
		//
		// @Override
		// public void run() {
		// doubleBackToExitPressedOnce = false;
		//
		// }
		// }, 2000);
	}

}
