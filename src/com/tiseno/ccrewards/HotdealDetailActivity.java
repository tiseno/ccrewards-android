package com.tiseno.ccrewards;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.ksoap2.serialization.SoapObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.ImageDownloaderTask;
import com.tiseno.ccrewards.webservicedata.MerchantHotDeal;
import com.tiseno.ccrewards.webservicedata.SoapResultParser;
import com.tiseno.ccrewards.webservicedata.WebServiceParsingTask;

public class HotdealDetailActivity extends SherlockActivity implements
		AsyncTaskCompleteListener<SoapObject> {
	private MerchantHotDeal merchantDetail = new MerchantHotDeal();
	private TextView tv_name, tv_descrip, tv_second_title, tv_promotion,
			tv_tel, tv_email, tv_website, tv_addr, tv_cardname;
	private ImageView image;
	private ImageView bt_share, bt_addFav;
	private LinearLayout ly_merchantDetails, ly_merchant, ly_tel, ly_email,
			ly_website, ly_addr, ly_promo, ly_cardname;
	private Context context;

	private String imageUrl;
	ProgressBar dialog;

	private SQLiteDatabase db;
	static DatabaseAssistant da;
	private DBConnection helper = new DBConnection(this, null);

	private List<BasicNameValuePair> propety = new ArrayList<BasicNameValuePair>();

	// button status
	boolean isFavourite = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hotdeal_detail);
		Global.setActionBar(this, getResources()
				.getString(R.string.deal_detail));
		context = HotdealDetailActivity.this;
		
		db = helper.getWritableDatabase();
		da = new DatabaseAssistant(db);

		try {
			Bundle bn = new Bundle();
			bn = getIntent().getExtras();
			Log.i(Global.TAG, "bn is null: " + (bn == null));
			if (bn.containsKey("merchantDetail")) {
				merchantDetail = (MerchantHotDeal) bn
						.getSerializable("merchantDetail");

				imageUrl = Global.IMAGE_URL + merchantDetail.merchantimage﻿;
				initView(merchantDetail);
				checkButtonStatus();
			} else if (bn.containsKey("merchantid")) {
				Log.i(Global.TAG, "gettting merchangid");
				propety.add(new BasicNameValuePair("merchantid", bn
						.getInt("merchantid") + ""));
				Log.i(Global.TAG,
						"gettting merchangid :" + bn.getInt("merchantid"));
				getHotdeals();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void checkButtonStatus() {
		isFavourite = da.getFavouriteByID(merchantDetail.merchantid);
		if (isFavourite) {
			bt_addFav.setImageDrawable(context.getResources().getDrawable(
					R.drawable.btn_favourites_active));
		} else {
			bt_addFav.setImageDrawable(context.getResources().getDrawable(
					R.drawable.bt_addfavorites));
		}
	}

	private void initView(MerchantHotDeal data) {
		tv_second_title = (TextView) findViewById(R.id.st_title);
		tv_second_title.setTypeface(Global.getTypeface(context,
				"HelveticaNeueLTStd-Bd.otf"));
		if (data.merchantbankname.equals(""))
			tv_second_title.setText(data.merchantname﻿);
		else
			tv_second_title.setText(data.merchantbankname);

		// get layout reference
				ly_merchantDetails = (LinearLayout) findViewById(R.id.merchantDetailsLayout);
				ly_merchant = (LinearLayout) findViewById(R.id.merchantLayout);
				ly_tel = (LinearLayout) findViewById(R.id.telLayout);
				ly_email = (LinearLayout) findViewById(R.id.emailLayout);
				ly_website = (LinearLayout) findViewById(R.id.websiteLayout);
				ly_addr = (LinearLayout) findViewById(R.id.addressLayout);
				ly_promo = (LinearLayout) findViewById(R.id.promoLayout);
				ly_cardname = (LinearLayout) findViewById(R.id.cardnameLayout);
				
		//card name
		if (data.merchantcardname.isEmpty() || data.merchantcardname.equals(" ") || data.merchantcardname.equals("-")) {
			ly_merchantDetails.removeView(ly_cardname);
		} else {
			tv_cardname = (TextView) findViewById(R.id.cardname_tv);
			tv_cardname.setText(data.merchantcardname);
		}
				
		// merchant name
		if (data.merchantname﻿.isEmpty() || data.merchantname﻿.equals(" ") || data.merchantname﻿.equals("-")) {
			ly_merchantDetails.removeView(ly_merchant);
		} else { 
			tv_name = (TextView) findViewById(R.id.hd_content_name);
			tv_name.setText(data.merchantname﻿);
		}
		
		// address
		if (data.merchantaddress.isEmpty() || data.merchantaddress.equals(" ") || data.merchantaddress.equals("-")) {
			ly_merchantDetails.removeView(ly_addr);
		} else { 
			tv_addr = (TextView) findViewById(R.id.hd_content_addr);
			tv_addr.setText(data.merchantaddress);
		}
		
		// merchant promotion/deal details
		tv_descrip = (TextView) findViewById(R.id.hd_content_descrip);
		tv_descrip.setText(data.merchantdesc﻿);
		
		// promowords
		if (data.merchantpromowords.isEmpty() || data.merchantpromowords.equals(" ") || data.merchantpromowords.equals("-")) {
			tv_promotion = (TextView) findViewById(R.id.hd_content_promotion);
			ly_promo.removeView(tv_promotion);
		} else { 
			tv_promotion = (TextView) findViewById(R.id.hd_content_promotion);
			tv_promotion.setText(data.merchantpromowords);
		}
			
		// tel
		if (data.merchanttel.isEmpty() || data.merchanttel.equals(" ") || data.merchanttel.equals("-")) {
			ly_merchantDetails.removeView(ly_tel);
		} else { 
			final String tel = data.merchanttel;
			tv_tel = (TextView) findViewById(R.id.hd_content_tel);
			tv_tel.setTextColor(getResources().getColor(R.color.blue));
			tv_tel.setText(Global.underline(tel));
			tv_tel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					Global.makeCall(HotdealDetailActivity.this,tel);
				}
			});
		}
		
		// email
		if (data.merchantemail.isEmpty() || data.merchantemail.equals(" ") || data.merchantemail.equals("-")) {
			ly_merchantDetails.removeView(ly_email);
		} else { 
			tv_email = (TextView) findViewById(R.id.hd_content_email);
			tv_email.setText(data.merchantemail);
		}
		
		// website
		if (data.merchanturl.isEmpty() || data.merchanturl.equals(" ") || data.merchanturl.equals("-")) {
			ly_merchantDetails.removeView(ly_website);
		} else { 
			tv_website = (TextView) findViewById(R.id.hd_content_website);
			tv_website.setTextColor(getResources().getColor(R.color.blue));
			final String url = data.merchanturl;
			tv_website.setText(Global.underline(url));
			tv_website.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					Global.openUrl(HotdealDetailActivity.this,url);

				}
			});
		}
		
		image = (ImageView) findViewById(R.id.hd_content_image);

		ImageDownloaderTask imageDownload = new ImageDownloaderTask(this,
				image, imageUrl);
		imageDownload.execute();

		ButtonListerner bl = new ButtonListerner(this);
		bl.setListeners();
		bl.setActiveButton(bl.HOTDEAL_other);

		bt_share = (ImageView) findViewById(R.id.st_iv_share);
		
		// share
		String shareMsg = merchantDetail.merchantname﻿ + "\n"
				+ merchantDetail.merchantpromowords + "\n"
				+ merchantDetail.merchanturl + "";
		bl.setShareMsg(shareMsg);
		bt_share.setOnClickListener(bl.ShareListener);

		bt_addFav = (ImageView) findViewById(R.id.st_iv_favourite);
		bt_addFav.setOnClickListener(FavouriteListener);
	}

	OnClickListener FavouriteListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (da != null) {
				if (!isFavourite) {
					ContentValues cv = new ContentValues();
					cv.put("merchantid", merchantDetail.merchantid);
					if (da.insertFavourite(cv)) {
						Toast.makeText(
								context,
								context.getResources().getString(
										R.string.add2fav), 1500).show();
					}
				} else {
					da.removeFavourite(merchantDetail.merchantid);
				}
			} else
				Log.i(Global.TAG, "da is null");
			checkButtonStatus();
		}
	};

	@Override
	public void onTaskComplete(SoapObject result) {
		for (int index = 0; index < result.getPropertyCount(); index++) {
			SoapObject childObj = (SoapObject) result.getProperty(index);
			SoapResultParser soapResult = new SoapResultParser();
			try {
				soapResult.parseDataObject(childObj.toString(), merchantDetail);
				imageUrl = Global.IMAGE_URL + merchantDetail.merchantimage﻿;
				initView(merchantDetail);
				checkButtonStatus();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			}
		}

		dialog.setVisibility(View.GONE);
	}

	private void getHotdeals() {
		if (Global.haveNetworkConnection(this)) {
			WebServiceParsingTask webTask = new WebServiceParsingTask(
					Global.WSDL_URL, Global.NAMESPACE,
					Global.SOAPMETHOD_GETMERH_BY_MERH_ID, propety,
					Global.SOAPACTION_GETMERH_BY_MERH_ID, this);

			webTask.execute();

			dialog = (ProgressBar) findViewById(R.id.progressBar);
			dialog.setVisibility(View.VISIBLE);
		}
	}
}