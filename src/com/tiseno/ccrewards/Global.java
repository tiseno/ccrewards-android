package com.tiseno.ccrewards;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.http.message.BasicNameValuePair;
import org.ksoap2.serialization.SoapObject;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.LayoutParams;
import com.actionbarsherlock.app.SherlockActivity;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.WebServiceParsingTask;

public class Global {
	public static String TAG = "Rewards2Go";

	public static String DOMAINNAME = "http://ccreward.com/";

	public static String IMAGE_URL = DOMAINNAME + "cms/images/";

	public static String WSDL_URL = DOMAINNAME + "cms/service.asmx?WSDL";

	public static String NAMESPACE = "http://tempuri.org/";

	public static String API_KEY = "AIzaSyA1SJTb9p_Fw2-8z5TXw7TjVPzmxpK55_Q";

	// soap method
	public static String SOAPMETHOD_INSERTSURVEY = "InsertSurvey";

	public static String SOAPMETHOD_HOTDEALS = "merchanthotdeals";

	public static String SOAPMETHOD_GETALLBANK = "getallbank";

	public static String SOAPMETHOD_GETCARDBYID = "getcardBybankid";

	public static String SOAPMETHOD_GETMERH_CAT_BY_ID = "getMerchantCategoryByCardid";

	public static String SOAPMETHOD_GETMERH_BY_CARD_CATID = "getmerchantBycategoryidcardid";

	public static String SOAPMETHOD_GETMERH_BY_MERH_ID = "getMerchantByMerchantid";

	public static String SOAPMETHOD_GETMERH_BY_KEYWORD = "searchmerchant";

	public static String SOAPMETHOD_GETFAVOURITE_MERCHANT = "favouritemerchant";

	public static String SOAPMETHOD_GETMYCARD = "GetMyCards";

	public static String SOAPMETHOD_INSERTTOKEN = "InsertAndroid";

	// soap action
	public static String SOAPACTION_INSERTSURVEY = NAMESPACE + "InsertSurvey";

	public static String SOAPACTION_HOTDEALS = NAMESPACE + "merchanthotdeals";

	public static String SOAPACTION_GETALLBANK = NAMESPACE + "getallbank";

	public static String SOAPACTION_GETCARDBYID = NAMESPACE + "getcardBybankid";

	public static String SOAPACTION_GETMERH_CAT_BY_ID = NAMESPACE
			+ "getMerchantCategoryByCardid";

	public static String SOAPACTION_GETMERH_BY_CARD_CATID = NAMESPACE
			+ "getmerchantBycategoryidcardid";

	public static String SOAPACTION_GETMERH_BY_MERH_ID = NAMESPACE
			+ "getMerchantByMerchantid";

	public static String SOAPACTION_GETMERH_BY_KEYWORD = NAMESPACE
			+ "searchmerchant";

	public static String SOAPACTION_GETFAVOURITE_MERCHANT = NAMESPACE
			+ "favouritemerchant";

	public static String SOAPACTION_GETMYCARD = NAMESPACE + "GetMyCards";

	public static String SOAPACTION_INSERTTOKEN = NAMESPACE + "InsertAndroid";

	//

	public static int TASK_COMPLET = 200;

	// SharePreferences tag
	public static String ALREADY_PASS_SURVEY = "alreadyPassSurvey";

	private Context context;

	public Global() {
		this.context = null;
	}

	public Global(Context context) {
		this.context = context;
	}

	// write to share preferences
	public boolean InsertSharePreferences(String key, String value) {
		SharedPreferences sp = context.getSharedPreferences("Rewards2Go",
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(key, value);
		return editor.commit();
	}

	public boolean InsertSharePreferences(String key, boolean value) {
		SharedPreferences sp = context.getSharedPreferences("Rewards2Go",
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putBoolean(key, value);
		return editor.commit();
	}

	// read to share preferences
	public String GetSharePreferences(String key, String defaultValue) {
		SharedPreferences sp = context.getSharedPreferences("Rewards2Go",
				Context.MODE_PRIVATE);
		return sp.getString(key, defaultValue);
	}

	public boolean GetSharePreferences(String key, boolean defaultValue) {
		SharedPreferences sp = context.getSharedPreferences("Rewards2Go",
				Context.MODE_PRIVATE);
		return sp.getBoolean(key, defaultValue);
	}

	public static String getDeviceID(Context context) {
		final TelephonyManager tm = (TelephonyManager) context
				.getApplicationContext().getSystemService(
						Context.TELEPHONY_SERVICE);

		final String tmDevice, tmSerial, androidId;
		tmDevice = "" + tm.getDeviceId();
		tmSerial = "" + tm.getSimSerialNumber();
		androidId = ""
				+ android.provider.Settings.Secure.getString(
						context.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		UUID deviceUuid = new UUID(androidId.hashCode(),
				((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
		String deviceId = deviceUuid.toString();
		return deviceId;
	}

	public String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}

	private String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}

	// init action bar for each intent
	public static void setActionBar(Context context, String title) {
		final ActionBar ab = ((SherlockActivity) context).getSupportActionBar();
		ab.setDisplayShowTitleEnabled(false);

		LayoutInflater inflater = LayoutInflater.from(context);
		View customView = inflater.inflate(R.layout.title_textview, null);

		TextView titleTV = (TextView) customView
				.findViewById(R.id.action_custom_title);
//		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
//				LayoutParams.WRAP_CONTENT);
//		layoutParams.setMargins(30, 5, 0, 0);
//		titleTV.setLayoutParams(layoutParams);
		titleTV.setText(title);
		titleTV.setTypeface(getTypeface(context, "Wisdom Script.otf"));

		ab.setCustomView(customView);
		ab.setDisplayShowCustomEnabled(true);

	}

	public static Typeface getTypeface(Context context, String fontname) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/"
				+ fontname);
		return tf;
	}

	//
	public static Bitmap getBitmapFromURL(String src) {
		try {
			Log.e("src", src);
			URL url = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			Log.e("Bitmap", "returned");
			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			Log.e("Exception", e.getMessage());
			return null;
		}
	}

	// get all favourite id
	public static String getFavList(DatabaseAssistant da) {
		Cursor c = da.getAllFavourite();
		StringBuffer str = new StringBuffer();
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
			str.append(c.getInt(c.getColumnIndex("merchantid")) + ",");
		if (str.length() > 0) {
			str.deleteCharAt(str.length() - 1);
		}

		return str.toString();
	}

	// get all card id
	public static String getCardList(DatabaseAssistant da) {
		Cursor c = da.getAllCard();
		StringBuffer str = new StringBuffer();
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
			str.append(c.getInt(c.getColumnIndex("cardid")) + ",");
		if (str.length() > 0) {
			str.deleteCharAt(str.length() - 1);
		}

		return str.toString();
	}

	// return true if device have network connect
	public static boolean haveNetworkConnection(Context ct) {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) ct
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		if (haveConnectedWifi || haveConnectedMobile)
			Log.i(TAG, "have network connected");
		else
			Toast.makeText(
					ct,
					ct.getResources().getString(
							R.string.no_network_connect_warninig), 1500).show();
		return haveConnectedWifi || haveConnectedMobile;
	}

	public static void insertToken(Context ct, String registrationId,
			AsyncTaskCompleteListener<SoapObject> callback) {
		List<BasicNameValuePair> propety = new ArrayList<BasicNameValuePair>();
		propety.add(new BasicNameValuePair("devicetoken", registrationId + ""));
		propety.add(new BasicNameValuePair("deviceid", getDeviceID(ct) + ""));
		WebServiceParsingTask webTask = new WebServiceParsingTask(
				Global.WSDL_URL, Global.NAMESPACE,
				Global.SOAPMETHOD_INSERTTOKEN, propety,
				Global.SOAPACTION_INSERTTOKEN, callback);

		webTask.execute();
	}

	public static SpannableString underline(String str) {
		SpannableString content = new SpannableString(str);
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		return content;
	}

	public static void makeCall(Context context, String number) {
		try {
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:" + number));
			context.startActivity(callIntent);
		} catch (ActivityNotFoundException e) {
			Log.i("phone call", "Call failed", e);
		}
	}

	public static void openUrl(Context context, String url) {
		if (!url.startsWith("https://") && !url.startsWith("http://")){
		    url = "http://" + url;
		}
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		context.startActivity(i);
	}
}
