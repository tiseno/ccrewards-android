package com.tiseno.ccrewards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.ksoap2.serialization.SoapObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.BankCardDetail;
import com.tiseno.ccrewards.webservicedata.ListViewImageLoaderTask;
import com.tiseno.ccrewards.webservicedata.SoapResultParser;
import com.tiseno.ccrewards.webservicedata.WebServiceParsingTask;

public class MyCardActivity extends SherlockActivity implements
		AsyncTaskCompleteListener<SoapObject> {
	private List<BasicNameValuePair> propety = new ArrayList<BasicNameValuePair>();
	private List<BankCardDetail> banksData = new ArrayList<BankCardDetail>();
	private List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();

	private int VIEW = 100, EDIT = 200;
//	private Context context;
	private Activity activity;
	private int action = VIEW;

	private ListView main_list;

	private ProgressBar dialog;
	ActionMode mMode;
	private boolean cancel = false;
	private boolean lockEdit = true;
	private ImageView guideImg;
	private boolean isResult = false;

	private SQLiteDatabase db;
	static DatabaseAssistant da;
	private DBConnection helper = new DBConnection(this, null);

	String[] from = { "title", "banktitle", "image" };
	int[] to = { R.id.mycard_title, R.id.mycard_banktitle, R.id.mycard_cardimg };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Global.setActionBar(this, getResources().getString(R.string.card));
		setContentView(R.layout.activity_favourite_mycard);
		this.activity = this;

		db = helper.getWritableDatabase();
		da = new DatabaseAssistant(db);

		if (Global.haveNetworkConnection(this))
			// initial view
			initView();
		super.onCreate(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.i("ccreward (onCreateOptionsMenu)--------------------------------", "isResult:" + isResult);
		menu.clear();
		if (isResult) {
			Log.i("ccreward (onCreateOptionsMenu -- inside if)--------------------------------", "isResult:" + isResult);
			menu.add("Edit").setShowAsAction(
					MenuItem.SHOW_AS_ACTION_IF_ROOM
							| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			isResult = false;
		}
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
//		menu.clear();
//		if (isResult) {
//			menu.add("Edit").setShowAsAction(
//					MenuItem.SHOW_AS_ACTION_IF_ROOM
//							| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
//			isResult = false;
//		}
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (!lockEdit) {
			mMode = startActionMode(new AnActionModeOfEpicProportions(activity,
					null));
			action = EDIT;
			cancel = false;
			showCheckBox();
		}
		return super.onOptionsItemSelected(item);
	}

	private final class AnActionModeOfEpicProportions implements
			ActionMode.Callback {

		public AnActionModeOfEpicProportions(Context context,
				ArrayList<Integer> checkList) {

		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			menu.clear();
			menu.add("Cancel").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			cancel = true;
			action = VIEW;
			mode.finish();
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			if (!cancel) {
				da.removeMyCards(getSelectCardID());
			}
			action = VIEW;
			LoadListView();
		}

		private List<Integer> getSelectCardID() {
			ArrayList<Integer> checkList = new ArrayList<Integer>();
			HashMap<String, Object> _data;

			for (int index = (data.size() - 1); index >= 0; index--) {
				_data = data.get(index);
				Boolean ischeck = (Boolean) _data.get("ischeck");
				if (ischeck) {
					checkList.add((Integer) _data.get("cardid"));
					data.remove(index);
					banksData.remove(index);
				}

				if (data.size() == 0)
					showGuideImage();
				else
					hideGuideImage();
			}
			return checkList;
		}
	}

	private void showCheckBox() {
		SimpleAdapter adapter = new SimpleAdapter(this, data,
				R.layout.button_mycard_list_check, from, to);
		main_list.setAdapter(adapter);
		main_list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		main_list.setItemsCanFocus(false);
	}

	@Override
	public void onTaskComplete(SoapObject result) {
		if (result != null) {
			data.clear();
			for (int index = 0; index < result.getPropertyCount(); index++) {
				SoapObject childObj = (SoapObject) result.getProperty(index);
				BankCardDetail _bankDetail = new BankCardDetail();
				SoapResultParser soapResult = new SoapResultParser();
				try {
					soapResult
							.parseDataObject(childObj.toString(), _bankDetail);
					banksData.add(_bankDetail);
					HashMap<String, Object> _data = new HashMap<String, Object>();
					_data.put("title", _bankDetail.cardname﻿);
					_data.put("banktitle", _bankDetail.cardbank);
					_data.put("ischeck", false);
					_data.put("cardid", _bankDetail.cardid﻿);
					_data.put("image", R.drawable.ic_nav_cards_active);		// default img
					_data.put("teaser_path", _bankDetail.cardimage﻿﻿﻿﻿﻿);

					data.add(_data);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InstantiationException e) {
					e.printStackTrace();
				}
			}
		}
		// dialog.dismiss();
		dialog.setVisibility(View.GONE);
		lockEdit = false;
		LoadListView();
	}

	private void LoadListView() {
		main_list = (ListView) findViewById(R.id.act_favourite_mycard_listview);
		main_list.setOnItemClickListener(ListClickListener);
		
		SimpleAdapter adapter = new SimpleAdapter(this, data,
				R.layout.button_mycard_list, from, to);
		main_list.setAdapter(adapter);
		
		if (adapter.getCount() != 0) {
			isResult = true;
			invalidateOptionsMenu();
		}
		
		for (int i = 0; i < adapter.getCount(); i++) {
			HashMap<String, Object> hm = (HashMap<String, Object>) adapter
					.getItem(i);
			String imgUrl = (String) hm.get("teaser_path");

			ListViewImageLoaderTask imageLoaderTask = new ListViewImageLoaderTask(
					this, main_list);

			HashMap<String, Object> hmDownload = new HashMap<String, Object>();
			String url = Global.IMAGE_URL + imgUrl;
			hm.put("teaser_path", url);
			hm.put("position", i);

			// Starting ImageLoaderTask to download and populate image in
			// the listview
			imageLoaderTask.execute(hm);
		}
	}

	private void initView() {
		ButtonListerner bl = new ButtonListerner(this);
		bl.setListeners();
		bl.setActiveButton(bl.MYCARD);

		guideImg = (ImageView) findViewById(R.id.guild_img);
		getAllCard();

	}

	private OnItemClickListener ListClickListener = new OnItemClickListener() {

		public void onItemClick(AdapterView adapterView, View view,
				int position, long id) {

			if (action == VIEW) {
				SimpleAdapter adapter = (SimpleAdapter) adapterView
						.getAdapter();

				adapter.notifyDataSetChanged();
				BankCardDetail bd = banksData.get(position);
				Intent intent = new Intent(activity,
						SearchMerchanCatActivity.class);
				intent.putExtra("cardid", bd.cardid﻿);
				intent.putExtra("cardname", bd.cardname﻿);
				startActivity(intent);
			} else if (action == EDIT) {
				CheckBox cb = (CheckBox) view
						.findViewById(R.id.bt_mycard_check);
				cb.toggle();
				data.get(position).put("ischeck", cb.isChecked());
				Log.i("TEST", data.get(position).get("banktitle") + " ischeck "
						+ cb.isChecked());
			}
		}
	};

	private void getAllCard() {
		String idList = Global.getCardList(da);
		Log.i("getAllCard (idList)--------------------", idList);
		if (!idList.equals("")) {
			hideGuideImage();
			propety.add(new BasicNameValuePair("listofcardid", idList));

			WebServiceParsingTask webTask = new WebServiceParsingTask(
					Global.WSDL_URL, Global.NAMESPACE,
					Global.SOAPMETHOD_GETMYCARD, propety,
					Global.SOAPACTION_GETMYCARD, this);

			webTask.execute();

			// dialog = ProgressDialog.show(this, "",
			// this.getResources().getString(R.string.loading_dialog), true);

			dialog = (ProgressBar) findViewById(R.id.progressBar);
			dialog.setVisibility(View.VISIBLE);
		} else {
			Log.i("MyCards -- getAllCard() ------------------", "showGuideImage");
			showGuideImage();
		}
	}

	private void showGuideImage() {
		guideImg.setVisibility(View.VISIBLE);
		guideImg.setImageDrawable(getResources().getDrawable(
				R.drawable.card_guide));
		isResult = false;
		invalidateOptionsMenu();
	}

	private void hideGuideImage() {
		guideImg.setVisibility(View.GONE);
	}

	@Override
	protected void onResume() {
		data.clear();
		this.onCreate(null);
		super.onResume();
	}

	// back press exit
	boolean doubleBackToExitPressedOnce = false;

	@Override
	public void onBackPressed() {
//		if (doubleBackToExitPressedOnce) {
		Intent intent = new Intent(this, HotDealActivity.class);
		this.startActivity(intent);
		this.finish();
//		super.onBackPressed();
		return;
//		}
//		this.doubleBackToExitPressedOnce = true;
//		Toast.makeText(this,
//				getResources().getString(R.string.press_back_alert),
//				Toast.LENGTH_SHORT).show();
//		new Handler().postDelayed(new Runnable() {
//
//			@Override
//			public void run() {
//				doubleBackToExitPressedOnce = false;
//
//			}
//		}, 2000);
	}

}
