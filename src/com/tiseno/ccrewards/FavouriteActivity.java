package com.tiseno.ccrewards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.ksoap2.serialization.SoapObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tiseno.ccrewards.webservicedata.AsyncTaskCompleteListener;
import com.tiseno.ccrewards.webservicedata.ListViewImageLoaderTask;
import com.tiseno.ccrewards.webservicedata.MerchantHotDeal;
import com.tiseno.ccrewards.webservicedata.SoapResultParser;
import com.tiseno.ccrewards.webservicedata.WebServiceParsingTask;

public class FavouriteActivity extends SherlockActivity implements
		AsyncTaskCompleteListener<SoapObject> {
	private ArrayList<MerchantHotDeal> merchantDetail = new ArrayList<MerchantHotDeal>();
	private ArrayList<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
	private List<BasicNameValuePair> propety = new ArrayList<BasicNameValuePair>();

	private int VIEW = 100, EDIT = 200; // action value
	private int action = VIEW;
	private boolean cancel = false; // is user click cancel on action bar edit
	private ListView main_list;
	private ProgressBar dialog;
	private ActionMode mMode;
	private Activity activity;

	private boolean lockEdit = true;
	private ImageView guideImg;
	private boolean isResult = false;
	private LinearLayout merchantlist_ly;

	// db work
	private SQLiteDatabase db;
	static DatabaseAssistant da;
	private DBConnection helper = new DBConnection(this, null);

	// adapter use
	String[] from = { "title", "promo", "image" };
	int[] to = { R.id.merchanlist_title, R.id.merchanlist_promo,
			R.id.merchanlist_im };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		Global.setActionBar(this, getResources().getString(R.string.favourite));
		setContentView(R.layout.activity_favourite_mycard);
		activity = this;

		db = helper.getWritableDatabase();
		da = new DatabaseAssistant(db);

		if (Global.haveNetworkConnection(this))
			// initial view
			initView();
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.i("ccreward (onCreateOptionsMenu)--------------------------------", "isResult:" + isResult);
		menu.clear();
		if (isResult) {
			Log.i("ccreward (onCreateOptionsMenu -- inside if)--------------------------------", "isResult:" + isResult);
			menu.add("Edit").setShowAsAction(
					MenuItem.SHOW_AS_ACTION_IF_ROOM
							| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			isResult = false;
		}
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
//		if (isResult) {
//			menu.add("Edit").setShowAsAction(
//					MenuItem.SHOW_AS_ACTION_IF_ROOM
//							| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
//			isResult = false;
//		}
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (!lockEdit) {
			mMode = startActionMode(new AnActionModeOfEpicProportions(activity,
					null));
			action = EDIT;
			cancel = false;
			showCheckBox();
		}
		return super.onOptionsItemSelected(item);
	}

	private final class AnActionModeOfEpicProportions implements
			ActionMode.Callback {

		public AnActionModeOfEpicProportions(Context context,
				ArrayList<Integer> checkList) {

		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			menu.clear();
			menu.add("Cancel").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			cancel = true;
			action = VIEW;
			mode.finish();
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			if (!cancel) {
				da.removeFavourites(getSelectMerchanID());
			}
			action = VIEW;
			LoadListView();
		}

		private List<Integer> getSelectMerchanID() {
			ArrayList<Integer> checkList = new ArrayList<Integer>();
			HashMap<String, Object> _data;
			for (int index = (data.size() - 1); index >= 0; index--) {
				_data = data.get(index);
				Boolean ischeck = (Boolean) _data.get("ischeck");
				if (ischeck) {
					checkList.add((Integer) _data.get("merchantid"));
					data.remove(index);
					merchantDetail.remove(index);
				}

				if (data.size() == 0) {
					showGuideImage();
				} else {
					hideGuideImage();
				}
			}
			return checkList;
		}
	}

	private void showCheckBox() {
		SimpleAdapter adapter = new SimpleAdapter(this, data,
				R.layout.button_simple_merchan_layout_check, from, to);
		main_list.setAdapter(adapter);
		main_list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		main_list.setItemsCanFocus(false);
	}

	@Override
	public void onTaskComplete(SoapObject result) {
		if (result != null) {
			data.clear();
			for (int index = 0; index < result.getPropertyCount(); index++) {
				SoapObject childObj = (SoapObject) result.getProperty(index);
				MerchantHotDeal merchantHotDeal = new MerchantHotDeal();
				try {
					SoapResultParser.parseDataObject(childObj.toString(),
							merchantHotDeal);
					merchantDetail.add(merchantHotDeal);
					HashMap<String, Object> _data = new HashMap<String, Object>();
					_data.put("name", merchantHotDeal.merchantname﻿);
					_data.put("title", merchantHotDeal.merchantname﻿);
					_data.put("promo", merchantHotDeal.merchantpromowords);
					_data.put("image", R.drawable.default_no_image);	// default img
					_data.put("ischeck", false);
					_data.put("merchantid", merchantHotDeal.merchantid);
					_data.put("teaser_path", merchantHotDeal.merchantteaser﻿﻿﻿);
					Log.i(Global.TAG, "childObj " + childObj.toString());
					data.add(_data);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		dialog.setVisibility(View.GONE);
		lockEdit = false;
		// dialog.dismiss();
		LoadListView();
	}

	private void LoadListView() {
		main_list = (ListView) findViewById(R.id.act_favourite_mycard_listview);
		main_list.setOnItemClickListener(ListClickListener);

		SimpleAdapter adapter = new SimpleAdapter(this, data,
				R.layout.button_simple_merchan_layout, from, to);
		main_list.setAdapter(adapter);

		if (adapter.getCount() != 0) {
			isResult = true;
			invalidateOptionsMenu();
		}
		
		for (int i = 0; i < adapter.getCount(); i++) {
			HashMap<String, Object> hm = (HashMap<String, Object>) adapter
					.getItem(i);
			String imgUrl = (String) hm.get("teaser_path");

			ListViewImageLoaderTask imageLoaderTask = new ListViewImageLoaderTask(
					this, main_list);

			HashMap<String, Object> hmDownload = new HashMap<String, Object>();
			String url = Global.IMAGE_URL + imgUrl;
			hm.put("teaser_path", url);
			hm.put("position", i);

			// Starting ImageLoaderTask to download and populate image in
			// the listview
			imageLoaderTask.execute(hm);
		}
	}

	private void initView() {
		ButtonListerner bl = new ButtonListerner(this);
		bl.setListeners();
		bl.setActiveButton(bl.FAVOIRITE);

		guideImg = (ImageView) findViewById(R.id.guild_img);
		getAllMerchan();
	}

	private OnItemClickListener ListClickListener = new OnItemClickListener() {

		public void onItemClick(AdapterView adapterView, View view,
				int position, long id) {

			if (action == VIEW) {
				SimpleAdapter adapter = (SimpleAdapter) adapterView
						.getAdapter();

				adapter.notifyDataSetChanged();
				MerchantHotDeal _category = merchantDetail.get(position);
				Intent intent = new Intent(activity,
						HotdealDetailActivity.class);
				intent.putExtra("merchantDetail", _category);
				startActivity(intent);
			} else if (action == EDIT) {
				CheckBox cb = (CheckBox) view
						.findViewById(R.id.bt_simple_merchan_ck);
				cb.toggle();
				data.get(position).put("ischeck", cb.isChecked());
			}
		}
	};

	private void getAllMerchan() {
		String idList = Global.getFavList(da);
		if (!idList.equals("")) {
			hideGuideImage();
			propety.add(new BasicNameValuePair("listofmerchantid", idList));

			WebServiceParsingTask webTask = new WebServiceParsingTask(
					Global.WSDL_URL, Global.NAMESPACE,
					Global.SOAPMETHOD_GETFAVOURITE_MERCHANT, propety,
					Global.SOAPACTION_GETFAVOURITE_MERCHANT, this);

			webTask.execute();

			// dialog = ProgressDialog.show(this, "",
			// this.getResources().getString(R.string.loading_dialog), true);
			// dialog.setCancelable(true);
			dialog = (ProgressBar) findViewById(R.id.progressBar);
			dialog.setVisibility(View.VISIBLE);
		} else {
			showGuideImage();
		}
	}

	private void showGuideImage() {
		guideImg.setVisibility(View.VISIBLE);
		guideImg.setImageDrawable(getResources().getDrawable(
				R.drawable.fav_guide));
		isResult = false;
		invalidateOptionsMenu();
	}

	private void hideGuideImage() {
		guideImg.setVisibility(View.GONE);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		data.clear();
		this.onCreate(null);
	}

	// back press exit
	boolean doubleBackToExitPressedOnce = false;

	@Override
	public void onBackPressed() {
//		if (doubleBackToExitPressedOnce) {
			Intent intent = new Intent(this, HotDealActivity.class);
			this.startActivity(intent);
			this.finish();
//			super.onBackPressed();
			return;
//		}
//		this.doubleBackToExitPressedOnce = true;
//		Toast.makeText(this,
//				getResources().getString(R.string.press_back_alert),
//				Toast.LENGTH_SHORT).show();
//		new Handler().postDelayed(new Runnable() {
//
//			@Override
//			public void run() {
//				doubleBackToExitPressedOnce = false;
//
//			}
//		}, 2000);
	}
}
